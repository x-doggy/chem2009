<#import "_partials/page.ftl" as page>

<@page.page>
	<h1 class="c-title-header">Теоретические основы химии</h1>
	<ul>	
	<#list published_posts?reverse as post>
		<#if (post.status == "published")>
			<li><a href="${post.uri}"><#escape x as x?xml>${post.title}</#escape></li>
		</#if>
	</#list>
	</ul>
</@page.page>