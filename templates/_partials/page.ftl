<#macro page>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <title><#if (content.title)??><#escape x as x?xml>${content.title}</#escape> | </#if>Учебник по химии</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Учебник по химии">
    <meta name="author" content="Vladimir Stadnik">
    <meta name="keywords" content="chemistry">
    <meta name="generator" content="JBake">

    <!-- Le styles -->
    <link href="<#if (content.rootpath)??>${content.rootpath}<#else></#if>css/common.css" rel="stylesheet">
    <link href="<#if (content.rootpath)??>${content.rootpath}<#else></#if>css/common-theme-blue.css" rel="stylesheet">

    <!-- Fav and touch icons -->
    <!--<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">-->
    <link rel="shortcut icon" href="<#if (content.rootpath)??>${content.rootpath}<#else></#if>favicon.ico">
  </head>
  <body class="c-body c-body--theme-blue">
    <div class="c-wrap" id="c-wrap">
      <div class="c-container">
        <#nested>
      </div>
    </div>
    <script src="<#if (content.rootpath)??>${content.rootpath}<#else></#if>js/NavigationElement.js"></script>
    <script src="<#if (content.rootpath)??>${content.rootpath}<#else></#if>js/ClockElement.js"></script>
    <script>
      new NavigationElement( document.getElementById("page-selection") );
      new ClockElement( document.getElementById("clock-interactive") );
    </script>
  </body>
</html>
</#macro>