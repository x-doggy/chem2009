/*
 * Navigation select element
 */

var NavigationElement = (function () {
  function visit(where) {
    window.location = where;
  }

  return function (element) {
    if (element) {
      element.addEventListener("change", function () {
        if (element.value) {
          visit(element.value);
        }
      });
    }
  };
})();
