/*
 * Clock text element
 */

var ClockElement = (function () {
  function formatDate(date) {
    var hours = date.getHours();
    var mins = date.getMinutes();
    var secs = date.getSeconds();

    if (hours < 10) hours = "0" + hours;
    if (mins < 10) mins = "0" + mins;
    if (secs < 10) secs = "0" + secs;

    return hours + ":" + mins + ":" + secs;
  }

  function clockTick(el) {
    el.innerText = formatDate(new Date());
  }

  return function (element) {
    if (element) {
      clockTick(element);
      setInterval(clockTick, 1000, element);
    }
  };
})();
